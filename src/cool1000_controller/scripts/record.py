#!/usr/bin/env python
'''
Please make sure that parallel servo node is running while record and play back


You can change the file name test.dat to rename the file
'''



import roslib
import rospy
from std_msgs.msg import Float64
from dynamixel_msgs.msg import JointState
#from dynamixel_msgs.msg import TorqueEnable

import time
import sys
import pickle


joint_names = (
               'joint1_controller',
               'dummy_joint2_controller',
               'dummy_joint8_controller',
               'joint3_controller',
               'joint4_controller',
               'joint5_controller',
               'joint6_controller',
               'joint7_controller',
               'gripper_open_controller_1',
		)

coords = []   # store an array of coordinates over time

def callback(data):
    #print data
    print data.motor_ids[0], data.current_pos
    coords.append(  [data.motor_ids[0], data.current_pos]  )


def listener():
    rospy.init_node('motion_record', anonymous=True)
    #for joint_name in joint_names:
    rospy.Subscriber("/joint1_controller/state", JointState, callback)
    rospy.Subscriber("/dummy_joint2_controller/state", JointState, callback)
    rospy.Subscriber("/dummy_joint8_controller/state", JointState, callback)
    rospy.Subscriber("/joint3_controller/state", JointState, callback)
    rospy.Subscriber("/joint4_controller/state", JointState, callback)
    rospy.Subscriber("/joint5_controller/state", JointState, callback)
    rospy.Subscriber("/joint6_controller/state", JointState, callback)
    rospy.Subscriber("/joint7_controller/state", JointState, callback)
    rospy.Subscriber("/gripper_open_controller_1/state", JointState, callback)
    rospy.spin()

if __name__ == '__main__':

  
           
    # begin recording joint angles
    
    time.sleep(1)

    print '-----------------------------------------------'
    print 'Listening'
    print '-----------------------------------------------'
    print ''

    listener()
    print 'Done listening'

    # Load the file where we will dump the position data array
    f = open('test.dat','w') 
    pickle.dump(coords,f)
    f.close()

    print 'Recording successful'

